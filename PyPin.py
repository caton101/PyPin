print("Getting sh!t done...")
debug = True
all_pins = [3, 5, 7, 8, 10, 11, 12, 13, 15, 16, 18, 19, 21, 22, 23, 24, 26, 29, 31, 32, 33, 35, 36, 37, 38, 40]

if debug:
        print("Importing modules...")
import RPi.GPIO as GPIO
from tkinter import *
from time import sleep

if debug:
        print("Loading Tkinter...")
master = Tk()

if debug:
        print("Setting Up GPIO Pin Mode...")
GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)
GPIO.setup(all_pins,GPIO.OUT)

if debug:
        print("Powering off GPIO Pins...")
GPIO.output(all_pins,1)

if debug:
        print("Creating list 'pins_on'...")
pins_on = []

if debug:
    print("Defining button functions...")
def callback(number,sender):
        if number == 711:
                GPIO.cleanup()
                exit()
        elif not number in pins_on:
                GPIO.output(number,0)
                pins_on.append(number)
                if len(pins_on) > 0:
                        print("The current pins on are: ")
                        for pin_on in pins_on:
                                print(pin_on)
                else:
                        print("No pins are on.")
                #master.sender.config(relief=SUNKEN)
        elif number in pins_on:
                GPIO.output(number,1)
                pins_on.remove(number)
                if len(pins_on) > 0:
                        print("The enabled pins are: ")
                        for pin_on in pins_on:
                                print(pin_on)
                else:
                        print("No pins are enabled.")
                #master.sender.config(relief=RAISED)
        else:
                print("ERROR: A tkinter button has sent illegal information to callback()!")

print("The buttons can be labled two different ways. Pick one:")
print("1. BCM Mode")
print("2. BOARD Mode")
mode = int(input("Enter number: "))
if mode == 1:
        if debug:
                print("Initializing Buttons...")
        master.btn_3 = Button(master, text="002",  command=lambda: callback(3,"btn_3")).grid(row=0,column=0)
        master.btn_5 = Button(master, text="003",  command=lambda: callback(5,"btn_5")).grid(row=0,column=1)
        master.btn_7 = Button(master, text="004",  command=lambda: callback(7,"btn_7")).grid(row=0,column=2)
        master.btn_29 = Button(master, text="005", command=lambda: callback(29,"btn_29")).grid(row=0,column=3)
        master.btn_31 = Button(master, text="006", command=lambda: callback(31,"btn_31")).grid(row=0,column=4)
        master.btn_26 = Button(master, text="007",  command=lambda: callback(26,"btn_26")).grid(row=0,column=5)
        master.btn_24 = Button(master, text="008",  command=lambda: callback(24,"btn_24")).grid(row=0,column=6)
        master.btn_21 = Button(master, text="009",  command=lambda: callback(21,"btn_21")).grid(row=0,column=7)
        master.btn_19 = Button(master, text="010",  command=lambda: callback(19,"btn_19")).grid(row=0,column=8)
        master.btn_23 = Button(master, text="011",  command=lambda: callback(23,"btn_23")).grid(row=1,column=0)
        master.btn_32 = Button(master, text="012", command=lambda: callback(32,"btn_32")).grid(row=1,column=1)
        master.btn_33 = Button(master, text="013", command=lambda: callback(33,"btn_33")).grid(row=1,column=2)
        master.btn_8 = Button(master, text="014",  command=lambda: callback(8,"btn_8")).grid(row=1,column=3)
        master.btn_10 = Button(master, text="015", command=lambda: callback(10,"btn_10")).grid(row=1,column=4)
        master.btn_36 = Button(master, text="016", command=lambda: callback(36,"btn_36")).grid(row=1,column=5)
        master.btn_11 = Button(master, text="017", command=lambda: callback(11,"btn_11")).grid(row=1,column=6)
        master.btn_12 = Button(master, text="018", command=lambda: callback(12,"btn_12")).grid(row=1,column=7)
        master.btn_35 = Button(master, text="019", command=lambda: callback(35,"btn_35")).grid(row=1,column=8)
        master.btn_38 = Button(master, text="020", command=lambda: callback(38,"btn_38")).grid(row=2,column=0)
        master.btn_40 = Button(master, text="021", command=lambda: callback(40,"btn_40")).grid(row=2,column=1)
        master.btn_15 = Button(master, text="022", command=lambda: callback(15,"btn_15")).grid(row=2,column=2)
        master.btn_16 = Button(master, text="023", command=lambda: callback(16,"btn_16")).grid(row=2,column=3)
        master.btn_18 = Button(master, text="024", command=lambda: callback(18,"btn_18")).grid(row=2,column=4)
        master.btn_22 = Button(master, text="025", command=lambda: callback(22,"btn_22")).grid(row=2,column=5)
        master.btn_37 = Button(master, text="026", command=lambda: callback(37,"btn_37")).grid(row=2,column=6)
        master.btn_13 = Button(master, text="027", command=lambda: callback(13,"btn_13")).grid(row=2,column=7)
        master.btn_end = Button(master, text="End", command=lambda: callback(711,"btn_end")).grid(row=2,column=8)
elif mode == 2:
        if debug:
                print("Initializing Buttons...")
        master.btn_3 = Button(master, text="003",  command=lambda: callback(3,"btn_3")).grid(row=0,column=0)
        master.btn_5 = Button(master, text="005",  command=lambda: callback(5,"btn_5")).grid(row=0,column=1)
        master.btn_7 = Button(master, text="007",  command=lambda: callback(7,"btn_7")).grid(row=0,column=2)
        master.btn_8 = Button(master, text="008",  command=lambda: callback(8,"btn_8")).grid(row=0,column=3)
        master.btn_10 = Button(master, text="010", command=lambda: callback(10,"btn_10")).grid(row=0,column=4)
        master.btn_11 = Button(master, text="011", command=lambda: callback(11,"btn_11")).grid(row=0,column=5)
        master.btn_12 = Button(master, text="012", command=lambda: callback(12,"btn_12")).grid(row=0,column=6)
        master.btn_13 = Button(master, text="013", command=lambda: callback(13,"btn_13")).grid(row=0,column=7)
        master.btn_15 = Button(master, text="015", command=lambda: callback(15,"btn_15")).grid(row=0,column=8)
        master.btn_16 = Button(master, text="016", command=lambda: callback(16,"btn_16")).grid(row=1,column=0)
        master.btn_18 = Button(master, text="018", command=lambda: callback(18,"btn_18")).grid(row=1,column=1)
        master.btn_19 = Button(master, text="019",  command=lambda: callback(19,"btn_19")).grid(row=1,column=2)
        master.btn_21 = Button(master, text="021",  command=lambda: callback(21,"btn_21")).grid(row=1,column=3)
        master.btn_22 = Button(master, text="022", command=lambda: callback(22,"btn_22")).grid(row=1,column=4)
        master.btn_23 = Button(master, text="023",  command=lambda: callback(23,"btn_23")).grid(row=1,column=5)
        master.btn_24 = Button(master, text="024",  command=lambda: callback(24,"btn_24")).grid(row=1,column=6)
        master.btn_26 = Button(master, text="026",  command=lambda: callback(26,"btn_26")).grid(row=1,column=7)
        master.btn_29 = Button(master, text="029", command=lambda: callback(29,"btn_29")).grid(row=1,column=8)
        master.btn_31 = Button(master, text="031", command=lambda: callback(31,"btn_31")).grid(row=2,column=0)
        master.btn_32 = Button(master, text="032", command=lambda: callback(32,"btn_32")).grid(row=2,column=1)
        master.btn_33 = Button(master, text="033", command=lambda: callback(33,"btn_33")).grid(row=2,column=2)
        master.btn_35 = Button(master, text="035", command=lambda: callback(35,"btn_35")).grid(row=2,column=3)
        master.btn_36 = Button(master, text="036", command=lambda: callback(36,"btn_36")).grid(row=2,column=4)
        master.btn_37 = Button(master, text="037", command=lambda: callback(37,"btn_37")).grid(row=2,column=5)
        master.btn_38 = Button(master, text="038", command=lambda: callback(38,"btn_38")).grid(row=2,column=6)
        master.btn_40 = Button(master, text="040", command=lambda: callback(40,"btn_40")).grid(row=2,column=7)
        master.btn_end = Button(master, text="End", command=lambda: callback(711,"btn_end")).grid(row=2,column=8)
else:
        GPIO.cleanup()
        exit()

if debug:
    print("Done!")
print("To exit use the End button")
