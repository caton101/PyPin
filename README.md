# PyPin

This is an old program I wrote for turning the pins of a Raspberry Pi 3 on and off. I was still a Python newbie when writing this so there are obviously flaws in my code. I have not modified the code since its original development in 2014.

**WARNING:** This program is no longer being maintained. USE AT YOUR OWN RISK!

**WARNING:** The code is very old and may not work on new versions of Python or its required modules.

## Usage

`python3 PyPin.py`